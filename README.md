# Payplus Package Laravel

## Description

Payplus is the easier plateform to perform online paiement with assurance

## Installation

```bash
$ composer require stephaneass/payplus
```

## For Laravel project, follow this tuto

**[Tuto for Laravel project](/forLaravelProjectREADME.md)**

## For other PHP project, follow this tuto

**[Tuto for other PHP project](/forOtherPhpProjectREADME.md)**

