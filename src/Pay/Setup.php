<?php
namespace StephaneAss\Payplus\Pay;

use StephaneAss\Payplus\Pay;

require_once dirname(__FILE__).'/conf.php';

class Setup extends Pay {

  private  $api_key;
  private  $token;

  private  $rootUrl = "";
  private  $rootTestUrl = "";

  private  $mode = "test";
  private  bool $with_redirect;

  public function __construct(){

      switch (_PAYMENT_PLATFORM) {
          case "payplus":
              $this->rootUrl = "https://app.payplus.africa";
              $this->rootTestUrl = "https://app.testthepayment.com";
              break;
          case "ligdicash":
              $this->rootUrl = "https://app.ligdicash.com";
              $this->rootTestUrl = "https://app.testthepayment.com";
              break;
          default:
              $this->rootUrl = "https://app.payplus.africa";
              $this->rootTestUrl = "https://app.testthepayment.com";
              break;
      }

      //$this->rootUrl = "http://localhost/applus/web";
      //$this->rootTestUrl = "http://localhost/applus/web";
  }

  public  function setApi_key($api_key) {
    $this->api_key = $api_key;
  }
  public  function setToken($token) {
    $this->token = $token;
  }
  public  function setMode($mode) {
    $this->mode = $mode;
  }
  public  function setWithRedirect(bool $with_redirect) {
    $this->with_redirect = $with_redirect;
  }

  public  function getApi_key() {
    return $this->api_key;
  }
  public  function getToken() {
    return $this->token;
  }
  public  function getMode() {
    return $this->mode;
  }
  public  function getWithRedirect() {
    return $this->with_redirect;
  }

  public  function getBaseUrl() {
    if ($this->getMode() == "live") {
      return $this->rootUrl;
    }else{
      return $this->rootTestUrl;
    }
  }

  public  function insert(Setup $Payplus_Setup){
      $this->setApi_key($Payplus_Setup->getApi_key());
      $this->setToken($Payplus_Setup->getToken());
      $this->setMode($Payplus_Setup->getMode());
      $this->setWithRedirect($Payplus_Setup->getWithRedirect());
  }

}