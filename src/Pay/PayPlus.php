<?php
namespace StephaneAss\Payplus\Pay;

use StephaneAss\Payplus\Pay\WithRedirect\Checkout\Invoice;
use StephaneAss\Payplus\Pay\WithoutRedirect\Invoice as WithoutRedirectInvoice;
use StephaneAss\Payplus\Pay\WithRedirect\Checkout\Store;

class PayPlus extends PayPlusConfig{

    public function __construct(bool $is_laravel_project = true)
    {
        if ($is_laravel_project) {
            $this->config();
        }
    }

    public function init(Store $store = null, Setup $setup = null, $with_redirect = true)
    {
        //Laravel project
        if (is_null($store) || is_null($setup)) {
            if (config("payplus.with_redirect")) {
                $this->setup->setWithRedirect(true);
                return new Invoice($this->store, $this->setup);
            }

            $this->setup->setWithRedirect(false);
            return new WithoutRedirectInvoice($this->store, $this->setup);
        }

        //Other PHP framework
        if ($with_redirect) {
            $setup->setWithRedirect(true);
            return new Invoice($store, $setup);
        }

        $setup->setWithRedirect(false);
        return new WithoutRedirectInvoice($store, $setup);
    }
}